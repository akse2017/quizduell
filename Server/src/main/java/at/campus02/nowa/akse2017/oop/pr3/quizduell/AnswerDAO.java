package at.campus02.nowa.akse2017.oop.pr3.quizduell;

import java.util.UUID;

public class AnswerDAO {

	private UUID id;
	private String text;

	public UUID getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public AnswerDAO(String text) {
		super();
		id = UUID.randomUUID();
		this.text = text;
	}

}
