package at.campus02.nowa.akse2017.oop.pr3.quizduell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.GameReady;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Login;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.OpponentReady;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.PlayerReady;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Question;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Results;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Results.Round;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Question.Answer;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.SelectedAnswer;

public class Client {

	public static void main(String[] args) {
		try (Socket client = new Socket(args[0], 1234);
				InputStream in = client.getInputStream();
				OutputStream out = client.getOutputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
			String name = null;
			System.out.println("Benutzername bitte:");
			while ((name = br.readLine()) != null) {
				if (name.trim().length() < 1) {
					System.out.println("Benutzername muss mindestens ein Zeichen enthalten!");
					continue;
				}
				break;
			}
			if (name == null) {
				System.out.println("Eingabe unterbrochen ... Ende!");
				return;
			}
			Login.newBuilder().setName(name.trim()).build().writeDelimitedTo(out);
			System.out.println("Warte auf Gegenspieler ...");
			OpponentReady opready = OpponentReady.parseDelimitedFrom(in);
			System.out.println("Gegenspieler bereit: " + opready.getName());
			if (opready.hasRules()) {
				System.out.println(opready.getRules());
			}
			System.out.println("Dr�cken Sie ENTER um zu starten ...");
			br.readLine();
			PlayerReady.newBuilder().build().writeDelimitedTo(out);
			System.out.println("Warten auf Start durch Gegenspieler ...");
			GameReady game = GameReady.parseDelimitedFrom(in);
			System.out.println("Spiel startet!");
			Map<String, Question> questions = new HashMap<>();
			for (int round = 0; round < game.getRounds(); round++) {
				Question question = Question.parseDelimitedFrom(in);
				questions.put(question.getId(), question);
				System.out.println("Runde " + (round + 1) + ": " + question.getQuestion());
				int key = 1;
				for (Answer answer : question.getAnswerList()) {
					System.out.println(" " + key + " " + answer.getText());
					key++;
				}
				System.out.println("Bitte Ziffer der richtigen Antwort eingeben:");
				Integer answer = null;
				while (answer == null) {
					String line = br.readLine();
					try {
						Integer temp = Integer.parseInt(line);
						if (temp >= 1 && temp <= question.getAnswerCount()) {
							answer = temp;
						}
					} catch (NumberFormatException e) {
						System.out.println("Keine g�ltige Ziffer!");
						continue;
					}
				}
				SelectedAnswer.newBuilder().setId(question.getAnswer(answer - 1).getId().toString()).build().writeDelimitedTo(out);
			}
			Results results = Results.parseDelimitedFrom(in);
			if (results.getScore() > results.getOpponent()) {
				System.out.println("GRATULATION: Sie haben gewonnen!");
			}
			System.out.println("Ihre erreichten Punkte: " + results.getScore());
			System.out.println("Gegenspieler: " + results.getOpponent());
			for (Round round : results.getRoundsList()) {
				Question q = questions.get(round.getQuestion());
				System.out.println(q.getQuestion());
				for (Answer a : q.getAnswerList()) {
					if (round.getAnswer().equals(a.getId())) {
						System.out.println("Richtig war: " + a.getText());
					}
				}
			}
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
