package at.campus02.nowa.akse2017.oop.pr3.quizduell;

public abstract class AbstractStoppableRunnable implements Runnable {

	private boolean isRunning = true;

	protected boolean shouldRun() {
		return isRunning;
	}
	
	public void requestShutdown() {
		isRunning = false;
	}
	
	
}
