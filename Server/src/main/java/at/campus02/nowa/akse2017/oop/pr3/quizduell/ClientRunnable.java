package at.campus02.nowa.akse2017.oop.pr3.quizduell;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.GameReady;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Login;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.OpponentReady;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.PlayerReady;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Question;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Question.Answer;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Question.Builder;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Results;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Results.Round;
import at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.SelectedAnswer;

public class ClientRunnable extends AbstractStoppableRunnable {

	private Socket client;
	private Set<QuestionDAO> questions = new HashSet<>();
	private int points = 0;

	public ClientRunnable(Socket client, List<QuestionDAO> pool) {
		super();
		this.client = client;
		Random rand = new Random();
		while (questions.size() < 6) {
			QuestionDAO candidate = pool.get(rand.nextInt(pool.size()));
			questions.add(candidate);
		}
	}

	@Override
	public void run() {
		try {
			client.setSoTimeout(200);
		} catch (SocketException e1) {
			e1.printStackTrace();
			return;
		}
		try (OutputStream out = client.getOutputStream(); InputStream in = client.getInputStream()) {
			Login login = Login.parseDelimitedFrom(in);
			System.out.println("Anmeldung: " + login.getName());
			OpponentReady.newBuilder().setName("A.I.")
					.setRules("Sie spielen derzeit alleine! Das Spiel dauert 6 Runden!").build().writeDelimitedTo(out);
			PlayerReady.parseDelimitedFrom(in);
			System.out.println(login.getName() + ": Spieler bereit!");
			GameReady.newBuilder().setRounds(6).build().writeDelimitedTo(out);
			for (QuestionDAO q : questions) {
				System.out.println(login.getName() + ": Frage gestellt: " + q.getText());
				System.out.println(login.getName() + ": Richtige Antwort w�re: " + q.getCorrect().getText());
				List<AnswerDAO> answers = new ArrayList<AnswerDAO>();
				answers.addAll(q.getWrong());
				answers.add(q.getCorrect());
				Collections.shuffle(answers);
				Builder builder = Question.newBuilder();
				builder.setQuestion(q.getText());
				builder.setId(q.getId().toString());
				for (AnswerDAO a : answers) {
					builder.addAnswer(Answer.newBuilder().setId(a.getId().toString()).setText(a.getText()).build());
				}
				builder.setTimer(0);
				builder.build().writeDelimitedTo(out);

				SelectedAnswer answer = SelectedAnswer.parseDelimitedFrom(in);
				AnswerDAO selected = null;
				for (AnswerDAO a : answers) {
					if (answer.getId().equals(a.getId().toString())) {
						selected = a;
						break;
					}
				}
				System.out.println(login.getName() + ": Antwort: " + selected.getText());
				if (UUID.fromString(answer.getId()).equals(q.getCorrect().getId())) {
					points++;
				}
				//answers.stream().filter(a -> Objects.equals(a.getId(), answer.getId())).findFirst();
				
			}
			at.campus02.nowa.akse2017.oop.pr3.quizduell.protocol.Quizduell.Results.Builder builder = Results
					.newBuilder().setScore(points).setOpponent(0);
			for (QuestionDAO q : questions) {
				Round round = Round.newBuilder().setQuestion(q.getId().toString())
						.setAnswer(q.getCorrect().getId().toString()).setOpponent("x").build();
				builder.addRounds(round);
			}
			builder.build().writeDelimitedTo(out);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
