package at.campus02.nowa.akse2017.oop.pr3.quizduell;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class QuestionDAO {
	
	private UUID id;
	private String text;
	private List<AnswerDAO> wrong = new ArrayList<>();
	private AnswerDAO correct;
	
	public QuestionDAO(String text) {
		super();
		this.id = UUID.randomUUID();
		this.text = text;
	}

	public UUID getId() {
		return id;
	}

	public AnswerDAO getCorrect() {
		return correct;
	}

	public void setCorrect(AnswerDAO correct) {
		this.correct = correct;
	}

	public String getText() {
		return text;
	}

	public List<AnswerDAO> getWrong() {
		return wrong;
	}
	
	public void addWrong(AnswerDAO answer) {
		wrong.add(answer);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionDAO other = (QuestionDAO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

}
