package at.campus02.nowa.akse2017.oop.pr3.quizduell;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


public class Server {

	public static void main(String[] args) {
		List<QuestionDAO> questions = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(Server.class.getResourceAsStream("quizfragen.csv"), StandardCharsets.UTF_8))) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] fields = line.replaceAll("\"", "").split(";");
				if (fields.length != 7) {
					continue;
				}
				/*if (!fields[6].matches("^[0-9]+$")) {
					continue;
				}*/
				try {
					Integer.parseInt(fields[6]);
				} catch (NumberFormatException e) {
					continue;
				}
				/*if (fields.length == 7 && fields[6].matches("...")) {
						
				}*/
				QuestionDAO question = new QuestionDAO(fields[0]);
				question.setCorrect(new AnswerDAO(fields[1]));
				question.addWrong(new AnswerDAO(fields[2]));
				question.addWrong(new AnswerDAO(fields[3]));
				question.addWrong(new AnswerDAO(fields[4]));
				questions.add(question);
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return;
		} catch (IOException e1) {
			e1.printStackTrace();
			return;
		}

		try {
			ServerSocket server = new ServerSocket(1234);
			server.setSoTimeout(200);
			while (true) {
				try {
					Socket client = server.accept();
					new Thread(new ClientRunnable(client, questions)).start();
				} catch (SocketTimeoutException e) {
					// Just for the loop-timeout. Do nothing!
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
